$headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
$headers.Add("Content-Type", "application/json")
$headers.Add("Authorization", "Token deb3b84120253caf5539e7ffed5188a520560fee")

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$response = Invoke-RestMethod 'https://test.cosmos.health/api/questionnaire/collection/' -Method 'GET' -Headers $headers
$questionSets = $response.results | Where-Object {$_.name -ne 'Manual questions'}#| ConvertFrom-Json

$html = ''
#$questionSets = Get-Content .\QuestionSyncConcept\questionsets.json | ConvertFrom-Json
foreach ($questionSet in $questionSets) {
    $html +="<b>" + $questionSet.name + ":</b>"
    $url = 'https://test.cosmos.health/api/questionnaire/question/twilio-tree/?collection=' + $questionSet.id
    $response = Invoke-RestMethod $url -Method 'GET' -Headers $headers
    $questions = $response.questions  | Where-Object {$_.parent_answer -eq $null}
    foreach ($question in $questions) {
        $html+=' '+$question.question
    }
    $html += '<br>'
}
Remove-Item .\QuestionSyncConcept\test.html
$html >> .\QuestionSyncConcept\test.html
#this is a test change
