﻿#Email Function
function sendMail ($strbody) {
$From = "cmrapp@cmredis.com"
$To ="cjoseph@cmredis.com,jlandry@cmredis.com"
$SMTPServer = "smtp.gmail.com"
$SMTPPort = "587"
$Username = "cmrapp@cmredis.com"
$Password = "SendEmail3390"
$subject =$strsubject
$smtp = New-Object System.Net.Mail.SmtpClient($SMTPServer, $SMTPPort);
$smtp.EnableSSL = $true
$smtp.Credentials = New-Object System.Net.NetworkCredential($Username, $Password);
$smtp.Send($From,$To,$subject,$body);
}

Function GenerateCharts($Today, $Yesterday) {

#DateTime Variables
#get previous dates using http://www.timeanddate.com/date/duration.html 
#The duration goes into the $Yesterday field

##File Structure DateTime DO NOT EDIT
$year = $Yesterday.Substring(6,4)
$month = $Yesterday.Substring(0,2)
$day = $Yesterday.Substring(3,2)

##SQL DateTime DO NOT EDIT
$SQLToday = $Today
$SQLToday2 = $Today.TrimEnd("00:00:00.000") + '03:45:00.000'
$SQLToday3 = $Today.TrimEnd("00:00:00.000") + '23:59:59.999'
$SQLYesterday = $Yesterday
$SQLYesterday2 = $Yesterday.TrimEnd("03:45:00.000") + '00:00:00.000'
$SQLYesterday3 = $Yesterday.TrimEnd("03:45:00.000") + '23:59:59.999'

#Local Directory Variables
$Chartdir="C:\Change_PDFCharts\$year\$month\$day\"
New-Item -ItemType Directory -Force -Path $chartdir



#PDFChart Documentation Creation
$Connection = new-object system.data.sqlclient.sqlconnection #Set new object to connect to sql database
$Connection.ConnectionString = "data source=cmrmcea2;Initial Catalog=CMRData;Persist Security Info=True;Asynchronous Processing=true;User ID=smarch;Password=theman"
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand 

$SqlQuery = @"

	SELECT tpr.EncID
FROM CMRDATA..tprencounters tpr
INNER JOIN cmrdata..tappeople tap ON tpr.personid = tap.personid
WHERE (
		(arrivaltime < '$SQLToday'
		AND ( 
			physiciandatetime > '$SQLYesterday'
			OR
			Nursedatetime >  '$SQLYesterday'
			)
		AND ( 
			physiciandatetime < '$SQLToday'
			OR
			Nursedatetime <  '$SQLToday'
			)
        AND EncStatusID = 2
		AND (nursedatetime is not null
		AND physiciandatetime is not null)
			)
            OR
			(arrivaltime > '$SQLYesterday'
			AND arrivaltime < '$SQLToday'
		AND ( 
			physiciandatetime < '$SQLYesterday'
			OR
			Nursedatetime <  '$SQLYesterday'
			))
		AND EncStatusID = 2
		AND (nursedatetime is not null
		AND physiciandatetime is not null)
			)
			Order by PtName
"@
Write-Output $SqlQuery
$Connection.open()
$SqlCmd.CommandText = $SqlQuery
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $SqlCmd
$SqlCmd.Connection = $Connection
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet)
$Connection.Close()
$uri = "http://cmrmcea2:8000/printservice.svc"
$reports = ("rptFaceSheet","rptNursingNotes","rptPhysicianChart","rptProFee")

$i=0
do
	{
	$proxy = New-WebServiceProxy -Uri $URI -Class PDFs -Namespace webservice
	$proxy.PrintEncountersByEncID($Reports,($DataSet.Tables[0].rows[$i]["encid"]),'True',"$Chartdir")
	$i++
	}
While($i -lt $DataSet.Tables[0].Rows.count)


#Chart & Addenda Documentation Rename
$SqlQuery3 = @"

		select Distinct A.encid,A.deptencid, B.PersonFirst, B.PersonSurname
		from cmrdata..tprencounters a
		left join cmrdata..tappeople b
		on a.personid = b.personid
		left join cmrdata..tpraddendum c
		on a.encid=c.encid
		WHERE (
		(arrivaltime < '$SQLToday'
		AND ( 
			physiciandatetime > '$SQLYesterday'
			OR
			Nursedatetime >  '$SQLYesterday'
			)
		AND ( 
			physiciandatetime < '$SQLToday'
			OR
			Nursedatetime <  '$SQLToday'
			)
            AND EncStatusID = 2
		AND (nursedatetime is not null
		AND physiciandatetime is not null)
			 )
			OR
			(arrivaltime > '$SQLYesterday'
			AND arrivaltime < '$SQLToday'
		AND ( 
			physiciandatetime < '$SQLYesterday'
			OR
			Nursedatetime <  '$SQLYesterday'
			))
		AND EncStatusID = 2
		AND (nursedatetime is not null
		AND physiciandatetime is not null)
			)
				
"@

$Connection.open()
$SqlCmd.CommandText = $SqlQuery3
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $SqlCmd
$SqlCmd.Connection = $Connection
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet)
$Connection.Close()

$PtData = $DataSet.Tables[0]
Foreach ($Patient in $PtData)
{
$VisitID = $Patient.deptencid
$EncID = $Patient.encid
#$VisitID
$IncludeFiles = ("*$VisitID*")
$files = Get-ChildItem $Chartdir -Filter $IncludeFiles

	Foreach ($File in $files)
	{
		if ($File.Name -like "*Addenda.pdf")
		{
		$Filename = "RPH_" +  $year + "-" + $month + "-" + $day + "_" + $patient.PersonSurname + "_" + $patient.PersonFirst + "_" + $patient.deptencid + "_rptAddendaHeader.pdf"
		$Filename
		Rename-Item $File.FullName $Filename
		}
		else
		{
		$Filename = "RPH_" + $year + "-" + $month + "-" + $day + "_" + $patient.PersonSurname + "_" + $patient.PersonFirst + "_" + $patient.deptencid + ".pdf"
        $Filename
        Rename-Item $File.FullName $Filename 
		}
	}
}
}

Function GenerateLog ($Today, $Yesterday, $Chartdir) {
##File Structure DateTime DO NOT EDIT
$year = $Yesterday.Substring(6,4)
$month = $Yesterday.Substring(0,2)
$day = $Yesterday.Substring(3,2)


##SQL DateTime DO NOT EDIT
$SQLToday = $Today
$SQLToday2 = $Today.TrimEnd("00:00:00.000") + '03:45:00.000'
$SQLToday3 = $Today.TrimEnd("00:00:00.000") + '23:59:59.999'
$SQLYesterday = $Yesterday
$SQLYesterday2 = $Yesterday.TrimEnd("03:45:00.000") + '00:00:00.000'
$SQLYesterday3 = $Yesterday.TrimEnd("03:45:00.000") + '23:59:59.999'
#Create Chart Log File
#Connect to SQL and run QUERY 
$SQLServer = "CMRMCEA2" 
$SQLDBName = "CMRData"
$SQLUsername = "smarch"
$SQLPassword = "theman"
 
$OuputFile = "$Chartdir\RPH_$year-$month-$day-ChartLog.csv"
write-output $OutputFile
 
$SqlQuery = "select 'Riverland' as 'Facility Name',
PersonFirst+' '+PersonSurname as 'Patient Name',
DeptPtId as 'Patient MRN',
DeptEncid as 'Patient Acct No',
PtDob as 'Patient DOB',
DATEDIFF(year,PtDob,Arrivaltime)as 'Patient Age',
PhysicianDisposition.PhysicianDisposition as 'Dispostion'
From CMRdata..tprEncounters
inner join cmrdata..tappeople on tprencounters.personid = tappeople.personid
inner join(
select encid, 'Discharge' AS PhysicianDisposition 
from cmrarchive..archivedisposition where encdatastatus = 2 and  fbformdefid = 17979
union
select encid, 'MSE DONE' AS PhysicianDisposition 
from cmrarchive..archivedisposition where encdatastatus = 2 and  fbformdefid = 72549
union
select encid, 'Admit' AS PhysicianDisposition 
from cmrarchive..archivedisposition where encdatastatus = 2 and  fbformdefid = 17980
union
select encid, 'Observation' AS PhysicianDisposition 
from cmrarchive..archivedisposition where encdatastatus = 2 and  fbformdefid = 73112
union
select encid, 'Transfer' AS PhysicianDisposition 
from cmrarchive..archivedisposition where encdatastatus = 2 and  fbformdefid = 17981
union
select encid, 'AMA' AS PhysicianDisposition 
from cmrarchive..archivedisposition where encdatastatus = 2 and  fbformdefid = 17982
union
select encid, 'Eloped' AS PhysicianDisposition 
from cmrarchive..archivedisposition where encdatastatus = 2 and  fbformdefid = 17983
union
select encid, 'Expired' AS PhysicianDisposition 
from cmrarchive..archivedisposition where encdatastatus = 2 and  fbformdefid = 17984
union
select encid, 'LWBS' AS PhysicianDisposition 
from cmrarchive..archivedisposition where encdatastatus = 2 and  fbformdefid = 17985
) PhysicianDisposition on tprEncounters.EncId = PhysicianDisposition.EncId
where arrivaltime < '$SqlToday' and arrivaltime > '$SqlYesterday'"
Write-Output $SqlQuery   
##Delete the output file if it already exists
If (Test-Path $OuputFile ){
    Remove-Item $OuputFile
}
  

## - Connect to SQL Server using non-SMO class 'System.Data': 
$SqlConnection = New-Object System.Data.SqlClient.SqlConnection 
$SqlConnection.ConnectionString = "Server = $SQLServer; Database = $SQLDBName; User ID = $SQLUsername; Password = $SQLPassword"
  
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand 
$SqlCmd.CommandText = $SqlQuery
$SqlCmd.Connection = $SqlConnection
#$SqlConnection.Open()  
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter 
$SqlAdapter.SelectCommand = $SqlCmd
  
$DataSet = New-Object System.Data.DataSet 
$SqlAdapter.Fill($DataSet) 
$SqlConnection.Close() 
 
#Output RESULTS to CSV
#$DataSet.Tables[0] | select "EmployeeNumber","JobTitle","Department","Company","Location","CostCentre","ManagerEmployeeNumber" | Export-Csv $OuputFile
$DataSet.Tables[0] | Export-Csv $OuputFile -NoTypeInformation
}