﻿function Get-SQL-Patients($dbuser, $dbpassword, $dbinstance,$SqlQuery) {
    $SqlConnection = New-Object System.Data.SqlClient.SqlConnection 
    $SqlConnection.ConnectionString = "Server = $dbinstance; User ID = $dbuser; Password = $dbpassword"
   # $SqlQuery = Get-Content "$PSScriptRoot\pull-patients.sql" #'select top 5 * from cmrdata..tprencounters order by encid desc'  
    $SqlCmd = New-Object System.Data.SqlClient.SqlCommand 
    $SqlCmd.CommandText = $SqlQuery
    $SqlCmd.Connection = $SqlConnection
    #$SqlConnection.Open()  
    $SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter 
    $SqlAdapter.SelectCommand = $SqlCmd
  
    $DataSet = New-Object System.Data.DataSet 
    $SqlAdapter.Fill($DataSet) 
    $SqlConnection.Close() 
    #$DataSet.Tables[0]
    $script:patients = $DataSet.Tables[0]
}

function Push-Patient-Event($uri, $token,$eventType,$mrn,$firstName,$lastName,$ptDob,$eventSummary,$eventSeverity,$dynamicParameters) {
    $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
    $headers.Add("Content-Type", "application/json")
    #$headers.Add("Authorization", "Token deb3b84120253caf5539e7ffed5188a520560fee")
    $headers.Add("Authorization", $token)

    $body = "{
`n    `"event-type`": `"Emergency Room Visit`",
`n    `"identifiers`": {
`n        `"patient-id`": `"`",
`n        `"follow-up-event-id`": `"`"
`n    },
`n    `"patient-data`": {
`n        `"medical-record-number`": `"$mrn`",
`n        `"first-name`": `"$firstName`",
`n        `"last-name`": `"$lastName`",
`n        `"phone-number`": `"`",
`n        `"pt-dob`": `"$ptDob`"
`n    },
`n    `"care_groups`": [
`n        3
`n    ],
`n    `"static-parameters`": {
`n        `"outside-url`": `"`",
`n        `"event-severity`": $eventSeverity,
`n        `"event-summary`": `"$eventSummary`"
`n    },
`n    `"dynamic-parameters`": $dynamicParameters,
`n    `"answers`": {}
`n}"
    $body
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    #$response = Invoke-RestMethod $uri -Method 'POST' -Headers $headers -Body $body
    #$response | ConvertTo-Json
    #$response.patient_id
    #$response
}

function Log-Patient-Event-Push($dbuser, $dbpassword, $dbinstance, $encid) {
    $SqlConnection = New-Object System.Data.SqlClient.SqlConnection 
    $SqlConnection.ConnectionString = "Server = $dbinstance; User ID = $dbuser; Password = $dbpassword"
    $SqlQuery = "INSERT INTO CosmosSync..cosmos_cmr_log" +
    "(encid,event_type,event_uploaded_to_cosmos,pdf_uploaded_to_cosmos) " +
    "VALUES($encid,'er visit',1,0)"  
    $SqlCmd = New-Object System.Data.SqlClient.SqlCommand 
    $SqlCmd.CommandText = $SqlQuery
    $SqlCmd.Connection = $SqlConnection
    $SqlConnection.Open()  
    #$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter 
    #$test = $SqlAdapter.in = $SqlCmd
    $test = $SqlCmd.ExecuteScalar()
    $test
    $SqlConnection.Close() 
}

function Push-Patient-PDF{

}

function sendMail ($strbody) {
    $From = $h.Get_Item('FromAddress')
    $To =$h.Get_Item('ToAddress')
    $SMTPServer = $h.Get_Item('SMTPServer')
    $SMTPPort = $h.Get_Item('SMTPPort')
    $Username = $h.Get_Item('MailUserName')
    $Password = $h.Get_Item('MailPassword')
    $subject ='This is a test email for now'
    $smtp = New-Object System.Net.Mail.SmtpClient($SMTPServer, $SMTPPort);
    $smtp.EnableSSL = $true
    $smtp.Credentials = New-Object System.Net.NetworkCredential($Username, $Password);
    $smtp.Se
    $smtp.Send($From,$To,$subject,$strbody);
    }