﻿###Purpose of this script:
###1)Get list of patients ER Visits that haven't been uploaded yet
###2)Upload to create event(will create patient automatically if they don't exist)
###3)On success update log table so that patient is marked completed, on failure mark as failed
###4)Generate PDF,Upload PDF, update log
###Synchronize reporting database is out of the scope of this script

#the first line loads configs from config.ini, the second line loads functions froom functions file
Get-Content -Path "$PSScriptRoot\config.ini" | foreach-object -begin { $h = @{} } -process { $k = [regex]::split($_, '='); if (($k[0].CompareTo("") -ne 0) -and ($k[0].StartsWith("[") -ne $True)) { $h.Add($k[0], $k[1]) } }
. "$PSScriptRoot\Functions.ps1"


###Get list of patients ER Visits that haven't been uploaded yet
###Need to update sql script once ready to, so that I get correct patient list and return correct data
###Need to add try and catch block, where catch sends helpdesk email and exits if pull patient fails
try {
    Get-CMR-Patients $h.Get_Item('DBUser') $h.Get_Item('DBPassword') $h.Get_Item('DBInstance')
}
catch {
    sendMail 'Failed to connect to sql server!'
    exit
}

foreach ($patient in $patients) {
    try {
        Post-Patient-Event $h.Get_Item('URI') $h.Get_Item('Token')
        Log-Patient-Event-Post $h.Get_Item('DBUser') $h.Get_Item('DBPassword') $h.Get_Item('DBInstance') $patient.EncId
    }
    catch {
        
    }
    try {
        Post-Patient-PDF $h.Get_Item('URI') $h.Get_Item('Token') 
    }
    catch {
        
    }
    
}

### Need to add below to foreach block above, before it logs the patient on catch should go to next patient, and send email after
### Need to add PDF
try {
    Post-Patient-Event $h.Get_Item('URI') $h.Get_Item('Token')
    $test = 'it worked, we processed patient with id - ' + $response.patient_id
    $response
    $test
}
catch {
    $response
    $failed = 'It failed'
    $failed
}