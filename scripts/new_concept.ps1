###Purpose of this script:
###1)Get list of patients ER Visits that haven't been uploaded yet
###2)Upload to create event(will create patient automatically if they don't exist)
###3)On success update log table so that patient is marked completed, on failure mark as failed
###4)Generate PDF,Upload PDF, update log
###Synchronize reporting database is out of the scope of this script

#the first line loads configs from config.ini, the second line loads functions froom functions file
Get-Content -Path "$PSScriptRoot\config.ini" | foreach-object -begin { $h = @{} } -process { $k = [regex]::split($_, '='); if (($k[0].CompareTo("") -ne 0) -and ($k[0].StartsWith("[") -ne $True)) { $h.Add($k[0], $k[1]) } }
. "$PSScriptRoot\Functions.ps1"


###Get list of patients ER Visits that haven't been uploaded yet
###Need to update sql script once ready to, so that I get correct patient list and return correct data
###Need to add try and catch block, where catch sends helpdesk email and exits if pull patient fails
$eventQueries = Get-ChildItem "$PSScriptRoot\Events" -Filter *.sql

### The idea is to use event type in sql name, and each column in query output will have a
### dynamic parameter with the same name
foreach ($eventQuery in $eventQueries)
{
    $queryText=Get-Content -Path "$PSScriptRoot\Events\$eventQuery"
    Get-SQL-Patients $h.Get_Item('DBUser') $h.Get_Item('DBPassword') $h.Get_Item('DBInstance') $queryText
    $eventType=$eventQuery.Name.Split(".")[0]
    $eventType
    foreach ($patient in $patients) {
        ### Below is how I will store as dynamic parameters - filtering fields that are standard event fields
        ### so i will pass below to function as well as $patient.firstname, etc.
        $dynamicParameters = $patient | Select-Object * -ExcludeProperty deptencid, deptptid, ItemArray, Table, RowError, RowState, HasErrors | ConvertTo-Json
        Push-Patient-Event $h.Get_Item('URI') $h.Get_Item('Token') $eventType  $patient.deptptid
        #function Push-Patient-Event($uri, $token,$eventType,$mrn,$firstName,$lastName,$ptDob,$eventSummary,$eventSeverity,$dynamicParameters) 
    }
}


try {
   # Get-CMR-Patients $h.Get_Item('DBUser') $h.Get_Item('DBPassword') $h.Get_Item('DBInstance')
}
catch {
    #sendMail 'Failed to connect to sql server!'
    #exit
}

foreach ($patient in $patients) {
    try {
        #Push-Patient-Event $h.Get_Item('URI') $h.Get_Item('Token')
        #Log-Patient-Event-Push $h.Get_Item('DBUser') $h.Get_Item('DBPassword') $h.Get_Item('DBInstance') $patient.EncId
    }
    catch {
        
    }
    try {
        #Push-Patient-PDF $h.Get_Item('URI') $h.Get_Item('Token') 
    }
    catch {
        
    }
    
}
